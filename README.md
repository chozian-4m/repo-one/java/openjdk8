# Description

RedHat OpenJDK 8 (JRE only). Supported through May 2026. For more information about OpenJDK life cycle and support please click [here](https://access.redhat.com/articles/1299013).

See [openjdk8-devel](https://repo1.dsop.io/dsop/opensource/openjdk/openjdk8-devel) for developer version.

# Environment Variables
This image has the following environment variables which can be used to customize behavior:
- **JAVA_HOME**: /usr/lib/jvm/jre-1.8.0-openjdk
