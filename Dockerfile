ARG BASE_REGISTRY=registry1.dsop.io
ARG BASE_IMAGE=redhat/ubi/ubi8
ARG BASE_TAG=8.5
FROM $BASE_REGISTRY/$BASE_IMAGE:$BASE_TAG


USER 0

RUN dnf update -y && \
    dnf install -y java-1.8.0-openjdk-headless && \
    dnf clean all && \
    rm -rf /var/cache/dnf

# Don't inherit a healthcheck from base image
HEALTHCHECK NONE

USER 1001

ENV LANG C.UTF-8
ENV JAVA_HOME /usr/lib/jvm/jre-1.8.0-openjdk
ENV PATH $JAVA_HOME/bin:$PATH
